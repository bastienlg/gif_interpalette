#!/usr/bin/env python3

import sys, argparse
import os.path
from PIL import Image
import numpy as np
from scipy.optimize import linear_sum_assignment as lsa


_exp = 2
def dist(col1, col2):
    return sum(abs(float(c1-c2))**_exp for c1,c2 in zip(col1,col2))

def avg(col1, col2):
    return tuple((c1+c2)//2 for c1,c2 in zip(col1,col2))

def interpalette_second(pal1, pal2):
    Dists = np.array([[dist(pal1[3*i:3*(i+1)], pal2[3*j:3*(j+1)]) for j in range(256)]
                      for i in range(256)])
    idx1,idx2 = lsa(Dists)
    for i,j in zip(idx1,idx2):
        pal2[3*j:3*(j+1)] = pal1[3*i:3*(i+1)]
    return pal2

def interpalette_both(pal1, pal2):
    Dists = np.array([[dist(pal1[3*i:3*(i+1)], pal2[3*j:3*(j+1)]) for j in range(256)]
                      for i in range(256)])
    idx1,idx2 = lsa(Dists)
    for i,j in zip(idx1,idx2):
        col = avg(pal1[3*i:3*(i+1)], pal2[3*j:3*(j+1)])
        pal1[3*i:3*(i+1)] = col
        pal2[3*j:3*(j+1)] = col
    return pal1,pal2


def reorder(pal1, pal2, img2):
    idx1 = {tuple(pal1[3*i:3*(i+1)]): i for i in range(256)}
    perm = np.array([idx1[tuple(pal2[3*j:3*(j+1)])] for j in range(256)], dtype=np.uint8)
    data2 = np.asarray(img2)
    data2 = perm[data2]
    nimg2 = Image.fromarray(data2, mode='P')
    nimg2.putpalette(pal1)
    return nimg2


def img_save(img, orig_name):
    out_name = 'out_' + os.path.basename(orig_name)
    print(f'Exporting to: {out_name}', file=sys.stderr)
    img.save(out_name)


def main():
    global _exp
    parser = argparse.ArgumentParser(description='Interpolate two GIF palettes')
    parser.add_argument('gif1', metavar='image1.gif')
    parser.add_argument('gif2', metavar='image2.gif')
    parser.add_argument('--degree', type=int, default=2,
                        help='exponent to use for the distance')
    parser.add_argument('--second', action='store_true',
                        help='only fit the second image to the first palette')
    parser.add_argument('--reorder', action='store_true',
                        help='also reorder the second palette')
    args = parser.parse_args()

    assert 0 < args.degree < 100, 'incorrect degree'
    _exp = args.degree

    img1 = Image.open(args.gif1)
    img2 = Image.open(args.gif2)
    pal1 = img1.getpalette()
    pal2 = img2.getpalette()
    assert len(pal1)==len(pal2)==3*256, 'incorrect palette size'

    if args.second:
        pal2 = interpalette_second(pal1, pal2)
        if args.reorder:
            img2 = reorder(pal1, pal2, img2)
        else:
            img2.putpalette(pal2)
        img_save(img2, args.gif2)
    else:
        pal1, pal2 = interpalette_both(pal1, pal2)
        img1.putpalette(pal1)
        if args.reorder:
            img2 = reorder(pal1, pal2, img2)
        else:
            img2.putpalette(pal2)
        img_save(img1, args.gif1)
        img_save(img2, args.gif2)


if __name__=='__main__':
    main()
